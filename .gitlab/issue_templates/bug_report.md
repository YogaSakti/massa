---
name: Bug report
about: Create a report to help us improve
---
<!-- ⚠️⚠️ Do Not Delete This! bug_report_template ⚠️⚠️ -->
<!--
Thank you for filing a bug report! 🐛 Please provide a short summary of the bug,
along with any information you feel relevant to replicating the bug.
-->
<!-- 🔎 Search existing issues to avoid creating duplicates. -->
<!-- 🧪 Test using the latest build to see if your issue has already been fixed -->

Steps to Reproduce:

1.
2.

I expected to see this happen: *explanation*

Instead, this happened: *explanation*

### Meta

- OS Version:

`rustc --version --verbose`:
```
<version>
```

<!--
Include a backtrace in the code block by setting `RUST_BACKTRACE=1` in your
environment. E.g. `RUST_BACKTRACE=1 cargo build`.
-->
<details><summary>Backtrace</summary>
<p>

```
<backtrace>
```

</p>
</details>
